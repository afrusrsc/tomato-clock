-- ----------------------------
-- file: Tomato.db
-- ver : SQLite3
-- ----------------------------

-- ----------------------------
-- 任务主表
-- ----------------------------
DROP TABLE IF EXISTS "Task";
CREATE TABLE "Task" (
  "ID" integer NOT NULL PRIMARY KEY AUTOINCREMENT,  --主键ID
  "TaskName" text NOT NULL DEFAULT '',              --任务名称
  "Remark" text NOT NULL DEFAULT '',                --备注，用于进一步详细描述任务
  "PreNum" integer NOT NULL DEFAULT 0,              --预计番茄钟个数
  "UsedNum" integer NOT NULL DEFAULT 0,             --实际使用番茄钟个数
  "InvalidNum" integer NOT NULL DEFAULT 0,          --作废番茄钟个数
  "State" integer NOT NULL DEFAULT 0,               --任务状态：0-未开始 1-进行中 2-已完成 3-作废
  "CreateTime" text NOT NULL                        --任务创建时间
);

-- ----------------------------
-- 番茄钟明细表
-- ----------------------------
DROP TABLE IF EXISTS "Tomato";
CREATE TABLE "Tomato" (
  "ID" integer NOT NULL PRIMARY KEY AUTOINCREMENT,  --主键ID
  "TaskID" integer NOT NULL,                        --任务ID
  "BeginTime" text,                                 --开始时间
  "EndTime" text,                                   --结束时间
  "Remark" text NOT NULL DEFAULT '',                --备注，用于描述番茄钟作废的原因
  "State" integer NOT NULL DEFAULT 0,               --番茄钟状态：0-工作中 1-工作完成 2-休息完成 3-作废
  "WorkLen" integer NOT NULL DEFAULT 0,             --工作时长
  "SleepLen" integer NOT NULL DEFAULT 0             --休息时长
);

-- ----------------------------
-- 杂项表
-- ----------------------------
DROP TABLE IF EXISTS "Misc";
CREATE TABLE "Misc" (
  "ID" integer NOT NULL PRIMARY KEY AUTOINCREMENT,  --主键ID
  "Kind" integer DEFAULT 0,                         --种类：0-作废理由
  "Content" text DEFAULT ''                        --内容
);