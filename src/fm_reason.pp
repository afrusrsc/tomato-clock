{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}
unit FM_Reason;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Menus, DBGridsEh, DB, SQLDB;

type

  { TfrmReason }

  TfrmReason = class(TForm)
    btnOK: TButton;
    ckbSave: TCheckBox;
    dsReason: TDataSource;
    grdReason: TDBGridEh;
    edtNewReason: TLabeledEdit;
    miDelReason: TMenuItem;
    pnlTool: TPanel;
    pmReason: TPopupMenu;
    qryReason: TSQLQuery;
    procedure btnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grdReasonDblClick(Sender: TObject);
    procedure miDelReasonClick(Sender: TObject);
  private
    FReason: string;
  public
    property Reason: string read FReason;
  end;

var
  frmReason: TfrmReason;

implementation

uses
  DM_Main;

{$R *.lfm}

{ TfrmReason }

procedure TfrmReason.FormShow(Sender: TObject);
begin
  dmMain.ShowReason(qryReason, 0);
end;

procedure TfrmReason.grdReasonDblClick(Sender: TObject);
begin
  if not qryReason.Active or (qryReason.RecordCount = 0) then
    Exit;
  edtNewReason.Text := qryReason.FieldByName('Content').AsString;
end;

procedure TfrmReason.btnOKClick(Sender: TObject);
begin
  if SameText(Trim(edtNewReason.Text), '') then
    Exit;
  FReason := Trim(edtNewReason.Text);
  if ckbSave.Checked then
    dmMain.NewReason(Reason);
  ModalResult := mrOk;
end;

procedure TfrmReason.miDelReasonClick(Sender: TObject);
begin
  if not qryReason.Active or (qryReason.RecordCount = 0) then
    Exit;
  if MessageDlg('删除提醒', '是否要删除该条理由？',
    mtWarning, mbYesNo, '') = mrYes then
    dmMain.DelReason(qryReason.FieldByName('ID').AsInteger);
end;

end.
