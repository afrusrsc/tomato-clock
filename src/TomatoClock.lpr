{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

program TomatoClock;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  uSysFunc,
  DM_Main,
  FM_Main,
  uConfig,
  FM_Config,
  FM_Tips,
  FM_TaskEdit,
  FM_TaskDetail,
  FM_Reason;

{$R *.res}

begin
  if RunAsSingle('{3A2A87FD-F470-456B-BC23-CFC3054A2C6F}') then
  begin
    RequireDerivedFormResource := True;
    Application.Title := '番茄钟';
    Application.Scaled := True;
    Application.Initialize;
    SetDefaultDateFormat;
    Application.CreateForm(TdmMain, dmMain);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end;
end.
