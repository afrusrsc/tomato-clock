{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

unit FM_TaskEdit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DM_Main;

type

  { TfrmTaskEdit }

  TfrmTaskEdit = class(TForm)
    btnSave: TButton;
    btnCancel: TButton;
    cbbState: TComboBox;
    edtTaskName: TLabeledEdit;
    edtPreNum: TLabeledEdit;
    lblRemark: TLabel;
    lblState: TLabel;
    mmoRemark: TMemo;
    procedure btnSaveClick(Sender: TObject);
    procedure edtPreNumKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    FMode: integer;
    FTask: TTask;
    //检查录入的数据
    function CheckData: boolean;
    procedure SetMode(AValue: integer);
  public
    //模式 0-新建 1-修改
    property Mode: integer read FMode write SetMode;
    //任务
    property Task: TTask read FTask write FTask;
  end;

var
  frmTaskEdit: TfrmTaskEdit;

implementation

{$R *.lfm}

{ TfrmTaskEdit }

procedure TfrmTaskEdit.FormShow(Sender: TObject);
begin
  edtTaskName.Text := Task.TaskName;
  edtPreNum.Text := IntToStr(Task.PreNum);
  mmoRemark.Text := Task.Remark;
  cbbState.ItemIndex := Task.State;
end;

function TfrmTaskEdit.CheckData: boolean;
begin
  Result := False;
  if SameText(Trim(edtTaskName.Text), '') then
  begin
    ShowMessage('任务名称不能为空');
    Exit;
  end;
  if StrToInt(edtPreNum.Text) = 0 then
  begin
    ShowMessage('预计钟数不能为 0');
    Exit;
  end;
  Result := True;
end;

procedure TfrmTaskEdit.edtPreNumKeyPress(Sender: TObject; var Key: char);
begin
  if not (Key in ['0'..'9', #8]) then
    Key := #0;
end;

procedure TfrmTaskEdit.btnSaveClick(Sender: TObject);
begin
  if not CheckData then
    Exit;
  case FMode of
    0: //新建
    begin
      FTask.TaskName := Trim(edtTaskName.Text);
      FTask.PreNum := StrToInt(edtPreNum.Text);
      FTask.Remark := Trim(mmoRemark.Text);
      FTask.ID := dmMain.NewTask(Task);
    end;
    1: //修改
    begin
      FTask.TaskName := Trim(edtTaskName.Text);
      FTask.PreNum := StrToInt(edtPreNum.Text);
      FTask.Remark := Trim(mmoRemark.Text);
      FTask.State := cbbState.ItemIndex;
      dmMain.ModfyTask(Task);
    end;
  end;
  ModalResult := mrOk;
end;

procedure TfrmTaskEdit.SetMode(AValue: integer);
begin
  FMode := AValue;
  case FMode of
    0: //新建
    begin
      Caption := '新建任务';
      edtTaskName.Enabled := True;
      cbbState.Enabled := False;
    end;
    1: //修改
    begin
      Caption := '修改任务';
      edtTaskName.Enabled := False;
      cbbState.Enabled := True;
    end;
  end;
end;

end.
