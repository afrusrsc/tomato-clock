{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}
unit FM_TaskDetail;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SQLDB, DB, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, DBGridsEh, DM_Main;

type

  { TfrmTaskDetail }

  TfrmTaskDetail = class(TForm)
    edtPreNum: TLabeledEdit;
    grdTomato: TDBGridEh;
    dsTomato: TDataSource;
    edtTaskName: TLabeledEdit;
    edtUsedNum: TLabeledEdit;
    edtInvalidNum: TLabeledEdit;
    pnlTask: TPanel;
    qryTomato: TSQLQuery;
    rgKind: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure rgKindClick(Sender: TObject);
  private
    FTask: TTask;
  public
    property Task: TTask read FTask write FTask;
  end;

var
  frmTaskDetail: TfrmTaskDetail;

implementation

{$R *.lfm}

{ TfrmTaskDetail }

procedure TfrmTaskDetail.FormShow(Sender: TObject);
begin
  edtTaskName.Text := Task.TaskName;
  edtUsedNum.Text := IntToStr(Task.UsedNum);
  edtInvalidNum.Text := IntToStr(Task.InvalidNum);
  edtPreNum.Text := IntToStr(Task.PreNum);
  rgKind.OnClick(rgKind);
end;

procedure TfrmTaskDetail.rgKindClick(Sender: TObject);
begin
  dmMain.ShowTaskDetail(qryTomato, Task.ID, rgKind.ItemIndex);
end;

end.
