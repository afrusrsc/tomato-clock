{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

unit FM_Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, SQLDB, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls, DBGridsEh, DM_Main;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    btnNewTask: TButton;
    btnModifyTask: TButton;
    btnDelTask: TButton;
    btnStartTomato: TButton;
    btnStopTomato: TButton;
    btnConfig: TButton;
    grdTask: TDBGridEh;
    dsTask: TDataSource;
    pnlTop: TPanel;
    pnlTimeView: TPanel;
    qryTask: TSQLQuery;
    tmrTimeSub: TTimer;
    procedure btnConfigClick(Sender: TObject);
    procedure btnDelTaskClick(Sender: TObject);
    procedure btnModifyTaskClick(Sender: TObject);
    procedure btnNewTaskClick(Sender: TObject);
    procedure btnStartTomatoClick(Sender: TObject);
    procedure btnStopTomatoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grdTaskDblClick(Sender: TObject);
    procedure tmrTimeSubTimer(Sender: TObject);
  private
    FStep: integer;
    mCurrTomatoID: integer;
    FTimeLen: integer;
    //显示当天任务
    procedure ShowTodayTask;
    //当前选择的任务
    function CurrTask: TTask;
    procedure SetStep(AValue: integer);
    procedure SetTimeLen(AValue: integer);
  public
    //时长
    property TimeLen: integer read FTimeLen write SetTimeLen;
    //步骤
    property Step: integer read FStep write SetStep;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  FM_Config, FM_Tips, FM_TaskEdit, FM_TaskDetail, FM_Reason;

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  if dmmain.Config.SaveFormPos then
  begin
    Self.Left := dmMain.Config.FormPos.X;
    Self.Top := dmMain.Config.FormPos.Y;
  end;
  Step := 0;
end;

procedure TfrmMain.btnConfigClick(Sender: TObject);
begin
  if not Assigned(frmConfig) then
    frmConfig := TfrmConfig.Create(Self);
  try
    frmConfig.ShowModal;
  finally
    FreeAndNil(frmConfig);
  end;
end;

procedure TfrmMain.btnDelTaskClick(Sender: TObject);
begin
  if qryTask.FieldByName('State').AsInteger = 3 then
    Exit;
  try
    if MessageDlg('提醒', '确定要作废该任务？', mtWarning,
      mbYesNo, '') = mrYes then
      dmMain.DelTask(qryTask.FieldByName('ID').AsInteger);
  finally
    ShowTodayTask;
  end;
end;

procedure TfrmMain.btnModifyTaskClick(Sender: TObject);
begin
  if not Assigned(frmTaskEdit) then
    frmTaskEdit := TfrmTaskEdit.Create(Self);
  try
    frmTaskEdit.Mode := 1;
    frmTaskEdit.Task := CurrTask;
    frmTaskEdit.ShowModal;
    ShowTodayTask;
  finally
    FreeAndNil(frmTaskEdit);
  end;
end;

procedure TfrmMain.btnNewTaskClick(Sender: TObject);
begin
  if not Assigned(frmTaskEdit) then
    frmTaskEdit := TfrmTaskEdit.Create(Self);
  try
    frmTaskEdit.Mode := 0;
    if frmTaskEdit.ShowModal = mrOk then
    begin
      Step := 0;
      ShowTodayTask;
    end;
  finally
    FreeAndNil(frmTaskEdit);
  end;
end;

procedure TfrmMain.btnStartTomatoClick(Sender: TObject);
begin
  if qryTask.RecordCount = 0 then
  begin
    ShowMessage('没有任务，不能开始！');
    Exit;
  end;
  if qryTask.FieldByName('State').AsInteger = 3 then
  begin
    ShowMessage('该任务已作废，不能继续！');
    Exit;
  end;
  if qryTask.FieldByName('State').AsInteger = 2 then
  begin
    ShowMessage('该任务已完成，无需继续！');
    Exit;
  end;
  mCurrTomatoID := dmMain.StartTomato(qryTask.FieldByName('ID').AsInteger);
  Step := Step + 1;
  ShowTodayTask;
end;

procedure TfrmMain.btnStopTomatoClick(Sender: TObject);
var
  Reason: string;
begin
  if Step = 1 then
  begin
    if not Assigned(frmReason) then
      frmReason := TfrmReason.Create(Self);
    try
      if frmReason.ShowModal = mrOk then
      begin
        Reason := frmReason.Reason;
        if SameText(Trim(Reason), '') then
        begin
          ShowMessage('理由为空，不能作废');
          Exit;
        end;
      end
      else
        Exit;
    finally
      FreeAndNil(frmReason);
    end;
    dmMain.StopTomato(mCurrTomatoID, Reason);
  end;
  if Step = 2 then
    dmMain.EndSleep(mCurrTomatoID);
  Step := 0;
  ShowTodayTask;
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if dmMain.Config.ExitMode = 0 then
    CloseAction := caHide;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  if dmMain.Config.SaveFormPos then
    dmMain.Config.FormPos := TPoint.Create(Self.Left, Self.Top);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  ShowTodayTask;
end;

procedure TfrmMain.grdTaskDblClick(Sender: TObject);
begin
  if not Assigned(frmTaskDetail) then
    frmTaskDetail := TfrmTaskDetail.Create(Self);
  frmTaskDetail.Task := CurrTask;
  frmTaskDetail.Show;
end;

procedure TfrmMain.tmrTimeSubTimer(Sender: TObject);
begin
  if TimeLen > 0 then
    TimeLen := TimeLen - 1
  else
    Step := Step + 1;
end;

procedure TfrmMain.ShowTodayTask;
begin
  dmMain.ShowTodayTask(qryTask);
  btnModifyTask.Enabled := qryTask.RecordCount > 0;
  btnDelTask.Enabled := qryTask.RecordCount > 0;
end;

procedure TfrmMain.SetStep(AValue: integer);
begin
  FStep := AValue;
  btnStartTomato.Enabled := FStep = 0;
  btnStopTomato.Enabled := FStep > 0;
  case FStep of
    0: //未开始
    begin
      tmrTimeSub.Enabled := False;
      TimeLen := 0;
    end;
    1: //工作
    begin
      TimeLen := dmMain.Config.WorkLen * 60;
      tmrTimeSub.Enabled := True;
    end;
    2: //休息
    begin
      tmrTimeSub.Enabled := False;
      dmMain.EndWork(mCurrTomatoID);
      TfrmTips.ShowTips(0);
      TimeLen := dmMain.Config.SleepLen * 60;
      tmrTimeSub.Enabled := True;
      ShowTodayTask;
    end;
    3: //完成
    begin
      tmrTimeSub.Enabled := False;
      dmMain.EndSleep(mCurrTomatoID);
      TfrmTips.ShowTips(1);
      ShowTodayTask;
      Step := 0;
    end;
  end;
end;

function TfrmMain.CurrTask: TTask;
begin
  Result.ID := qryTask.FieldByName('ID').AsInteger;
  Result.TaskName := qryTask.FieldByName('TaskName').AsString;
  Result.Remark := qryTask.FieldByName('Remark').AsString;
  Result.PreNum := qryTask.FieldByName('PreNum').AsInteger;
  Result.UsedNum := qryTask.FieldByName('UsedNum').AsInteger;
  Result.InvalidNum := qryTask.FieldByName('InvalidNum').AsInteger;
  Result.State := qryTask.FieldByName('State').AsInteger;
end;

procedure TfrmMain.SetTimeLen(AValue: integer);
begin
  if FTimeLen = AValue then
    Exit;
  FTimeLen := AValue;
  pnlTimeView.Caption := Format('%.2d:%.2d', [FTimeLen div 60, FTimeLen mod 60]);
end;

end.
