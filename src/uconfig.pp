{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

unit uConfig;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IniFiles;

type

  { TConfig }

  TConfig = class
  private
    FExitMode: integer;
    FFormPos: TPoint;
    FSaveFormPos: boolean;
    FSleepLen: integer;
    FTipsTimes: integer;
    FWorkLen: integer;
    mFile: string;
    FPath: string;
    procedure SetFormPos(AValue: TPoint);
    procedure SetTipsTimes(AValue: integer);
  public
    constructor Create;
    destructor Destroy; override;
    //加载配置
    procedure Load;
    //保存配置
    procedure Save;
  public
    property Path: string read FPath;
    //窗体位置
    property FormPos: TPoint read FFormPos write SetFormPos;
    //是否保存窗体位置
    property SaveFormPos: boolean read FSaveFormPos write FSaveFormPos;
    //提醒次数
    property TipsTimes: integer read FTipsTimes write SetTipsTimes;
    //工作时长
    property WorkLen: integer read FWorkLen write FWorkLen;
    //休息时长
    property SleepLen: integer read FSleepLen write FSleepLen;
    //退出模式 0:最小化 1:退出
    property ExitMode: integer read FExitMode write FExitMode;
  end;

implementation

const
  DEFUALT_TIPSTIMES = 3; //默认提醒次数

{ TConfig }

procedure TConfig.SetFormPos(AValue: TPoint);
begin
  if FFormPos = AValue then
    Exit;
  FFormPos := AValue;
end;

procedure TConfig.SetTipsTimes(AValue: integer);
begin
  if AValue < 1 then
    AValue := DEFUALT_TIPSTIMES;
  if FTipsTimes = AValue then
    Exit;
  FTipsTimes := AValue;
end;

constructor TConfig.Create;
begin
  mFile := ChangeFileExt(ParamStr(0), '.ini');
  FPath := ExtractFilePath(ParamStr(0));
end;

destructor TConfig.Destroy;
begin
  inherited Destroy;
end;

procedure TConfig.Load;
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(mFile);
  try
    //窗体位置
    FSaveFormPos := ini.ReadBool('FormPos', 'SaveFormPos', False);
    FFormPos.X := ini.ReadInteger('FormPos', 'Left', 0);
    FFormPos.Y := ini.ReadInteger('FormPos', 'Top', 0);
    //提醒
    TipsTimes := ini.ReadInteger('Tips', 'TipsTimes', DEFUALT_TIPSTIMES);
    //时长
    WorkLen := ini.ReadInteger('TimeLen', 'WorkLen', 25);
    SleepLen := ini.ReadInteger('TimeLen', 'SleepLen', 5);
    //退出模式
    ExitMode := ini.ReadInteger('System', 'ExitMode', 0);
  finally
    ini.Free;
  end;
end;

procedure TConfig.Save;
var
  ini: TIniFile;
begin
  ini := TIniFile.Create(mFile);
  try
    //窗体位置
    ini.WriteBool('FormPos', 'SaveFormPos', FSaveFormPos);
    if FSaveFormPos then
    begin
      ini.WriteInteger('FormPos', 'Left', FFormPos.X);
      ini.WriteInteger('FormPos', 'Top', FFormPos.Y);
    end;
    //提醒
    ini.WriteInteger('Tips', 'TipsTimes', TipsTimes);
    //时长
    ini.WriteInteger('TimeLen', 'WorkLen', WorkLen);
    ini.WriteInteger('TimeLen', 'SleepLen', SleepLen);
    //退出模式
    ini.WriteInteger('System', 'ExitMode', ExitMode);
  finally
    ini.Free;
  end;
end;

end.
