{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

unit FM_Config;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TfrmConfig }

  TfrmConfig = class(TForm)
    btnSave: TButton;
    btnCancel: TButton;
    cbSaveFormPos: TCheckBox;
    edtTipsTimes: TLabeledEdit;
    edtWorkLen: TLabeledEdit;
    edtSleepLen: TLabeledEdit;
    rgExitMode: TRadioGroup;
    procedure btnSaveClick(Sender: TObject);
    procedure edtTipsTimesKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    //显示配置
    procedure ShowConfig;
    //应用配置
    procedure ApplyConfig;
  public

  end;

var
  frmConfig: TfrmConfig;

implementation

uses
  DM_Main;

{$R *.lfm}

{ TfrmConfig }

procedure TfrmConfig.FormShow(Sender: TObject);
begin
  ShowConfig;
end;

procedure TfrmConfig.btnSaveClick(Sender: TObject);
begin
  ApplyConfig;
  ModalResult := mrOk;
end;

procedure TfrmConfig.edtTipsTimesKeyPress(Sender: TObject; var Key: char);
begin
  if not (Key in ['0'..'9', #8]) then
    Key := #0;
end;

procedure TfrmConfig.ShowConfig;
begin
  cbSaveFormPos.Checked := dmMain.Config.SaveFormPos;
  edtTipsTimes.Text := IntToStr(dmMain.Config.TipsTimes);
  edtWorkLen.Text := IntToStr(dmMain.Config.WorkLen);
  edtSleepLen.Text := IntToStr(dmMain.Config.SleepLen);
  rgExitMode.ItemIndex := dmMain.Config.ExitMode;
end;

procedure TfrmConfig.ApplyConfig;
begin
  dmMain.Config.SaveFormPos := cbSaveFormPos.Checked;
  dmMain.Config.TipsTimes := StrToInt(edtTipsTimes.Text);
  dmMain.Config.WorkLen := StrToInt(edtWorkLen.Text);
  dmMain.Config.SleepLen := StrToInt(edtSleepLen.Text);
  dmMain.Config.ExitMode := rgExitMode.ItemIndex;
end;

end.
