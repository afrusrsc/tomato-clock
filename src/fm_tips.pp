{*******************************************************************************
*  Copyright (c) 2021 Jesse Jin Authors. All rights reserved.                  *
*                                                                              *
*  Use of this source code is governed by a MIT-style                          *
*  license that can be found in the LICENSE file.                              *
*                                                                              *
*  版权由作者 Jesse Jin 所有。                                                 *
*  此源码的使用受 MIT 开源协议约束，详见 LICENSE 文件。                        *
*******************************************************************************}

unit FM_Tips;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  SpeechLib_5_4_TLB;

type

  { TfrmTips }

  TfrmTips = class(TForm)
    asvTTS: TAxcSpVoice;
    butOK: TButton;
    lblTips: TLabel;
    tmrClose: TTimer;
    tmrShake: TTimer;
    procedure butOKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure tmrCloseTimer(Sender: TObject);
    procedure tmrShakeTimer(Sender: TObject);
  private
    FKind: integer;
    FTimes: integer;
    //语音提醒
    procedure SetKind(AValue: integer);
    procedure VoiceTips;
  public
    //显示提醒
    class procedure ShowTips(AKind: integer);
    //提醒次数
    property Times: integer read FTimes write FTimes;
    //提醒种类
    property Kind: integer read FKind write SetKind;
  end;

implementation

uses
  DM_Main;

{$R *.lfm}

var
  frmTips: TfrmTips;

const
  TipsStr: array[0..1] of string =
    ('时间到，请注意休息！', '时间到，请开始工作！');

const
  DX = 10; //抖动幅度
  DTimes = 3; //抖动圈数

{ TfrmTips }

procedure TfrmTips.butOKClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTips.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  tmrShake.Enabled := False;
  tmrClose.Enabled := False;
  tmrShake.Tag := 0;
end;

procedure TfrmTips.FormShow(Sender: TObject);
begin
  Times := dmMain.Config.TipsTimes;
  tmrShake.Tag := 0;
  tmrShake.Enabled := True;
  VoiceTips;
end;

procedure TfrmTips.tmrCloseTimer(Sender: TObject);
begin
  tmrClose.Enabled := False;
  Times := Times - 1;
  if Times <= 0 then
    Close
  else
    tmrShake.Enabled := True;
end;

procedure TfrmTips.tmrShakeTimer(Sender: TObject);
begin
  case tmrShake.Tag mod 4 of
    0:
      Self.Left := Self.Left + DX;
    1:
      Self.Top := Self.Top + DX;
    2:
      Self.Left := Self.Left - DX;
    3:
    begin
      Self.Top := Self.Top - DX;
      if tmrShake.Tag mod (4 * DTimes) = (4 * DTimes - 1) then
      begin
        tmrShake.Enabled := False;
        tmrClose.Enabled := True;
      end;
    end;
  end;
  tmrShake.Tag := tmrShake.Tag + 1;
end;

procedure TfrmTips.VoiceTips;
begin
  asvTTS.OleServer.Speak('番茄种提醒您：' + lblTips.Caption, SVSFlagsAsync);
end;

procedure TfrmTips.SetKind(AValue: integer);
begin
  FKind := AValue;
  lblTips.Caption := TipsStr[FKind];
end;

class procedure TfrmTips.ShowTips(AKind: integer);
begin
  if not Assigned(frmTips) then
    frmTips := TfrmTips.Create(nil);
  frmTips.Kind := AKind;
  frmTips.Show;
end;

initialization

finalization
  if Assigned(frmTips) then
    FreeAndNil(frmTips);

end.
